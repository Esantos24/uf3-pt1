**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

Aquesta pràctica tracta de fer utilitzan els fitxers diferentes utilitats, hem de fer una opció d'alta, una de baixa, llistats tant en consola com en HTML, una de modificació i una de "compactar" per eliminar les dades del registre de l'Animal.

```
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

//CONSTANTS
#define MAX_TIPUS 50
#define MAX_NOM 50
//Per buidar el buffer
#define BuidaBuffer while(getchar()!='\n');

//Estructura de la que guardem les dades
typedef struct{
	char tipus[MAX_TIPUS+1];
	char nom[MAX_NOM+1];
	char sexe;// m-masculi, f-femeni
	bool enPerillExtincio;
	int edat;
	double pes;
	char marcaEsborrat;// asterisc vol dir esborrat!s
} Animal;

//Declaració de Accions i Funcions
void menu();
void entrarOpcio(int *opcio);
void accio (int opcio);
void pausa();
int alta();
void demanarClau (char nom[MAX_NOM]);
int baixa();
int llistar();
int llistarHTML();
int modificacio();
void entrarAnimal(Animal *ani);
void escriureAnimal(Animal ani);

int main()
{
    int opcio;
    do{
        menu();
        entrarOpcio(&opcio);
        accio(opcio);
        pausa();
    }while(opcio!=0);

    return 0;
}

//Mostrar el menú per consola
void menu(){
    printf("\n  **************************\n");
    printf("  ********** MENU **********\n");
    printf("  **************************\n");
    printf("  **  \t   1 - Alta\t  **\n");
    printf("  **  \t   2 - Baixa\t  **\n");
    printf("  ** 3 - Llistat Pantalla **\n");
    printf("  **   4 - Llistat Web\t  **\n");
    printf("  **   5 - Modificacio\t  **\n");
    printf("  **************************\n");
    printf("  ** \t   0 - Surt\t  **\n");
}

//Segons la opció introduida realitzar una funció o una altra
void accio (int opcio){
    switch(opcio){
        case 1:
            alta();
            break;

        case 2:
            baixa();
            break;

        case 3:
            llistar();
            break;

        case 4:
            llistarHTML();
            break;

        case 5:
            modificacio();
            break;

        case 0:
            break;

    }
}

//Introduir i guardar la opció
void entrarOpcio (int *opcio){
    printf("\n  Introdueix una opcio (0-5): ");
    scanf("%i",opcio);
    BuidaBuffer;
}

// Acció per a llimpiar la consola
void pausa(){
    char pausa;
    printf("\nPrem una tecla per continuar ...");
    scanf("%c", &pausa);
    BuidaBuffer;
    system("clear || cls");
}

//Introduir les dades de l'animal.
void entrarAnimal(Animal *ani){
    char siOno;
    printf("\nIntrodueix el tipus: ");
    scanf("%50[^\n]",ani->tipus);
    BuidaBuffer;
    printf("\nIntrodueix el nom: ");
    scanf("%50[^\n]",ani->nom);
    BuidaBuffer;
    do{
        printf("\nIntrodueix el sexe (m/f): ");
        scanf("%c",&ani->sexe);
        BuidaBuffer;
    }while(ani->sexe!='m' && ani->sexe!='f');
    do{
        printf("\nEsta en perill d'extincio? (s/n): ");
        scanf("%c",&siOno);
    }while(siOno!='s' && siOno!='n');
    if(siOno=='s'){
            ani->enPerillExtincio=true;
    }else{
        ani->enPerillExtincio=false;
    }
    BuidaBuffer;
    printf("\nIntrodueix la edat: ");
    scanf("%i",&ani->edat);
    BuidaBuffer;
    printf("\nIntrodueix el pes: ");
    scanf("%lf",&ani->pes);
    BuidaBuffer;
    ani->marcaEsborrat=' ';
}

//Mostrar les dades de l'animal.
void escriureAnimal(Animal ani){
    printf("\n******************\n");
    printf("\nEl tipus: %s\n",ani.tipus);
    printf("\nEl nom: %s\n",ani.nom);
    printf("\nEl sexe: %c\n",ani.sexe);
     if(ani.enPerillExtincio==true){
            printf("\nEsta en perill d'extincio\n");
    }else{
        printf("\nNo esta en perill d'extincio\n");
    }
    printf("\nLa edat: %i\n",ani.edat);
    printf("\nEl pes: %lf\n",ani.pes);
}

int alta(){
    FILE *f1;
    int n;
    Animal a;
    /*Obrir  el fitxer en mode append (anexar), ja que si l'obrim en mode "w"
     (write) cada vegada que guardéssim informació en lloc de guardar-se tota
      la introduïda s'aniria matxacant.*/
    f1= fopen("animals.dat","ab");
    if( f1 == NULL ) {
        printf("\nError en obrir el fitxer ...\n");
        return -1;
    }
    entrarAnimal(&a);
    /*Assignem a la variable n el nombre d'elements que escribim en el fitxer,
      a la funció fwrite li passem l'adreça de l'animal, el tamany, el nombre d'emenets
      i el fitxer on s'escriu.*/
    n= fwrite(&a,sizeof(Animal),1,f1);
    if(n==0) {
        printf("\nError d'escriptura!\n");
        return -2;
    }
    //Tanquem el fitxer.
    fclose(f1);

    return 0;
}

//Introduir i guardar la clau per si volem donar de baixa la informació d'un animal
void demanarClau (char nom[MAX_NOM]){
    printf("\nIntrodueix el nom de l'animal que vols donar de baixa o modificar: ");
    scanf("%50[^\n]",nom);
    BuidaBuffer;
}

//Donar de baixa animal
int baixa(){
    char nom[MAX_NOM+1];
    Animal a;
    FILE *f;
    int n;
    demanarClau(nom);
    //Obrir el fitxer animals.dat en mode "r" (lectura) i amb el "+" i afegim la escriptura
    f=fopen("animals.dat", "rb+");
    if(f==NULL){
        printf("\nError en obrir el fitxer ...\n");
        return -1;
    }
    /*Mentres no sigui final de fitxer assignem a la variable n el nombre d'elements que llegim d'el fitxer,
      a la funció fread li passem l'adreça de l'animal, el tamany, el nombre d'emenets
      i el fitxer d'on llegeix.*/
    while(!feof(f)){
        n = fread(&a,sizeof(Animal), 1,f);
        /*Si no és final mirem si n, que te guardat el nº d'elements que llegim és 0, si ho és vol dir
          que no s'ha llegit bé i mostrariam un error i retornariam el "codi d'error" -3*/
        if(!feof(f)){
            if(n==0){
                printf("\nError de lectura!\n");
                return -3;
            }
            /* Si l'animal no te la marca d' esborrat i el nom que li passem quan demanem la clau
               coincideix amb el nom de l'animal.*/
            if(a.marcaEsborrat!='*' && strcmp(a.nom,nom)==0){
                /*Ens movem una posició endarrera ja que després de fer el fread estem a la pròxima
                  posició i fiquem la marca d'esborrat i escrivim.*/
                fseek(f, -(long)sizeof(Animal), SEEK_CUR);
                a.marcaEsborrat='*';
                fwrite(&a, sizeof(Animal),1, f);
                /*Mirem si n, que te guardat el nº d'elements que llegim és 0, si ho és vol dir
                  que no s'ha escrit bé i mostrariam un missatge d'error i retornariam el "codi d'error" -2*/
                if(n==0){
                    printf("\nError d'escriptura!\n");
                    return -2;
                }
                break;
            }
        }
    }
    //Tanquem fitxer.
    fclose(f);

    return 0;
}

int llistar(){
    FILE *f2;
    int n;
    Animal a2;
     //obrir per llegir amb binari. El fitxer ha d'existir.
    f2= fopen("animals.dat","rb");
    if( f2 == NULL ) {
        printf("\nError en obrir el fitxer!\n");
        return -1;
    }
    /*Mentres no sigui final de fitxer assignem a la variable n el nombre d'elements que llegim d'el fitxer,
      a la funció fread li passem l'adreça de l'animal, el tamany, el nombre d'emenets
      i el fitxer d'on llegeix.*/
    while(!feof(f2)){
        n = fread(&a2,sizeof(Animal), 1,f2);
        if(!feof(f2)){
            if(n==0) {
                printf("\nError de lectura!\n");
                return -3;
            }
            //Si l'animal no té la marca d'esborrat mostrarem per pantalla les seves dades.
            if(a2.marcaEsborrat!='*'){
                escriureAnimal(a2);
            }
        }
    }
    //tanquem fitxer
    fclose(f2);

    return 0;
}

int llistarHTML(){
     //Codi estàtic d'HTML abans del cos.
     char capcalera[]="<!DOCTYPE html> \
                      <html lang=\"es\"> \
                      <head> \
                        <meta charset=\"UTF-8\"> \
                        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
                        <link rel=\"stylesheet\" href=\"reset.css\"> \
                        <link rel=\"stylesheet\" href=\"styles.css\"> \
                      </head> \
                      <body class=\"app\"> \
                        <header class=\"header\"> \
                            <h1 class=\"title\"> Llistat d' Animals</h1> \
                        </header> \
                        <article class=\"article\"> \
                        <table class=\"table\"> \
                          <tr> \
                              <th>Tipus</th> \
                              <th>Nom</th> \
                              <th>Sexe</th> \
                              <th>Perill d'extincio?</th> \
                              <th>Edat</th> \
                              <th>Pes (kg)</th> \
                          </tr>";

    //Cos on emmagatzemem les dades dels animals introduits.
    char cos[10000];

    //Codi estàtic d'HTML després del cos.
    char peu[]="</table> \
                </article> \
                <footer class=\"footer\"> \
                    <h4>Practica UF3 &nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp; Eric Santos Cortada</h4> \
                </footer> \
        </body> \
        </html>";

    FILE *f3;
     int n;
    char nTemp[10];
    Animal a3;
    cos[0]='\0';
     //obrir per llegir amb binari. El fitxer ha d'existir.
    f3= fopen("animals.dat","rb");
    if( f3 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }
    /*Mentres no sigui final de fitxer assignem a la variable n el nombre d'elements que llegim d'el fitxer,
      a la funció fread li passem l'adreça de l'animal, el tamany, el nombre d'emenets
      i el fitxer d'on llegeix.*/
    while(!feof(f3)){
        n = fread(&a3,sizeof(Animal), 1,f3);
        if(!feof(f3)){
            if(n==0) {
                printf("Error de lectura");
                return -3;
            }
            //Si no tenen la marca d'esborrat.
            if(a3.marcaEsborrat!='*'){
                /*Concatenar el cos a la taula, primer concatenarem el <tr> (fila) i despres anem obrint les
                cel·les, concatenem les dades i les tanquem, i així successivament. La funció strcpy copia el
                que li introduim a la variable que passem al paràmetre i la funció sprintf la fem servir per a
                passar les dades int i double a cadena*/
                strcat(cos,"<tr><td>");
                strcat(cos,a3.tipus);
                strcat(cos,"</td><td>");
                strcat(cos,a3.nom);
                strcat(cos,"</td><td>");
                if(a3.sexe=='m') strcpy(nTemp,"M");
                else strcpy(nTemp,"F");
                strcat(cos,nTemp);
                strcat(cos,"</td><td>");
                if(a3.enPerillExtincio) strcpy(nTemp,"Si");
                else strcpy(nTemp,"No");
                strcat(cos,nTemp);
                strcat(cos,"</td><td>");
                sprintf(nTemp,"%d",a3.edat);
                strcat(cos,nTemp);
                strcat(cos,"</td><td>");
                sprintf(nTemp,"%lf",a3.pes);
                strcat(cos,nTemp);
                strcat(cos,"</td></tr>");
            }
        }
    }
    //Tanquem fitxer.
    fclose(f3);

    f3=fopen("Informe.html","w");
    if(f3==NULL){
        printf("Error en obrir el fitxer informe.html");
        return -1;
    }
    if(fputs(capcalera,f3)==EOF){
        printf("error en escriure el fitxer informe.html");
        return -2;
    }
    llistar(cos);
    if(fputs(cos,f3)==EOF){
        printf("error en escriure el fitxer informe.html");
        return -2;
    }
    if(fputs(peu,f3)==EOF){
        printf("error en escriure el fitxer informe.html");
        return -2;
    }
    fclose(f3);
    system("firefox Informe.html");

    return 0;
}
```
